package com.aeffle.bitbucket.rest;

import com.atlassian.bitbucket.hamcrest.PageRequestMatcher;
import com.atlassian.bitbucket.project.Project;
import com.atlassian.bitbucket.project.ProjectService;
import com.atlassian.bitbucket.util.Page;
import com.atlassian.bitbucket.util.PageRequest;
import com.atlassian.bitbucket.util.PageUtils;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.bitbucket.repository.Repository;
import com.atlassian.bitbucket.repository.RepositoryService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


@Path("/repo")
public class RepositoryInfoResource {
    private RepositoryService repositoryService;

    public RepositoryInfoResource(RepositoryService repositoryService) {
        this.repositoryService = repositoryService;
    }

    @GET
    @Path("")
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response getRepositories() {
        PageRequest pageRequest = PageUtils.newRequest(0, 3000);
        Iterable<Repository> repos = repositoryService.findAll(pageRequest).getValues();

        List<Map<String, Object>> data = new LinkedList<Map<String, Object>>();
        for (Repository repo : repos) {
            data.add(getDataObject(repo));
        }

        return Response
                .ok(data).build();
    }


    @GET
    @Path("{repositoryId}")
    @AnonymousAllowed
    @Produces({MediaType.APPLICATION_JSON})
    public Response getRepository(@PathParam("repositoryId") Integer repositoryId) {
        Repository repository = getRepoInfo(repositoryId);

        if (repository == null) {
            return Response
                    .status(Response.Status.NOT_FOUND)
                    .entity("Repository does not exist.")
                    .build();
        }

        return Response.ok(getDataObject(repository)).build();
    }


    private Repository getRepoInfo(Integer repositoryId) {
        return repositoryService.getById(repositoryId);
    }

    private Map<String, Object> getDataObject(Repository repository) {
        Map<String, Object> data = new HashMap<String, Object>();

        data.put("id", repository.getId());
        data.put("project", repository.getProject().getKey());
        data.put("repo", repository.getSlug());
        data.put("size", repositoryService.getSize(repository));

        return data;
    }
}