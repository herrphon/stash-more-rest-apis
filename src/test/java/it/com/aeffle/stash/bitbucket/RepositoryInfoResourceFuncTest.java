package it.com.aeffle.stash.bitbucket;

import org.junit.Test;
import org.junit.After;
import org.junit.Before;

import static org.hamcrest.Matchers.isEmptyString;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

import org.apache.wink.client.Resource;
import org.apache.wink.client.RestClient;

import java.util.Map;

public class RepositoryInfoResourceFuncTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test
    public void messageIsValid() {
        String baseUrl = System.getProperty("baseurl");
        String resourceUrl = baseUrl + "/bitbucket/repo/1.0/more/repo/1";

        RestClient client = new RestClient();
        Resource resource = client.resource(resourceUrl);

        Map<String, Object> repo = resource.get(Map.class);

        assertThat((Integer)repo.get("id"), is(Integer.valueOf(1)));
        assertThat((String)repo.get("project"), not(isEmptyString()));
    }
}
